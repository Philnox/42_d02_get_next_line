gcc ./get_next_line/get_next_line.c ./get_next_line/libft/libft.a -I./get_next_line main.c 


gcc main_super.c ./get_next_line/get_next_line.c ./get_next_line/libft/libft.a -I./get_next_line
./a.out


gcc main_super_new.c ./get_next_line/get_next_line.c ./get_next_line/libft/libft.a -I./get_next_line
./a.out


clear && find ./get_next_line -maxdepth 1 -type f | xargs norminette

valgrind --leak-check=full ./a.out


### Size of stack ###
ulimit -s

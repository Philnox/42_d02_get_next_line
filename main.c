#include <stdio.h>
#include <fcntl.h>
#include "get_next_line.h"
#include<sys/syslimits.h>

int main(void) {
	char *line;
	ft_putstr("OPEN_MAX=");
	ft_putnbr(OPEN_MAX);
	ft_putstr("\n");

	/*
	int fd = open("gnl7_2.txt", O_RDONLY);
	while (get_next_line(fd, &line) == 1)
	{
		printf("%s\n", line);
		free(line);
	}
	printf("after=%s\n", line);
	*/

	while (1)
	{
		get_next_line(0, &line);
		printf("%s\n", line);
		free(line);
	}
	return (0);
}
